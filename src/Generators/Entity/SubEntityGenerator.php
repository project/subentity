<?php

namespace Drupal\subentity\Generators\Entity;

use DrupalCodeGenerator\Command\ModuleGenerator;
use DrupalCodeGenerator\Utils;

/**
 * Implements subentity command.
 */
final class SubEntityGenerator extends ModuleGenerator {

  /**
   * {@inheritdoc}
   */
  protected string $name = 'entity:subentity';

  /**
   * {@inheritdoc}
   */
  protected string $alias = 'subentity';

  /**
   * {@inheritdoc}
   */
  protected string $description = 'Generates a subentity.';

  /**
   * {@inheritdoc}
   */
  protected string $templatePath = __DIR__ . '/../../../templates/';

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $this->collectDefault($vars);

    $vars['label'] = $this->ask('Entity label', '{name}');
    $vars['entity_class'] = $this->ask('Class name', '{label|camelize}');
    $vars['bundle'] = $this->confirm('Has bundle?', FALSE);
    $vars['entity_name'] = Utils::human2machine($vars['entity_class']);

    $this->addFile(
      'src/Entity/{entity_class}.php',
      'generator/subentity.php.twig'
    );

    $this->addFile(
      '{machine_name}.links.menu.yml',
      'generator/links.menu.yml.twig'
    )->prependIfExists();

    $this->addFile(
      '{machine_name}.links.task.yml',
      'generator/links.task.yml.twig'
    )->prependIfExists();

    $this->addFile(
      '{machine_name}.routing.yml',
      'generator/routing.yml.twig'
    )->prependIfExists();

    if ($vars['bundle']) {
      $this->addFile(
        'src/Entity/{entity_class}Type.php',
        'generator/subentity_type.php.twig'
      );

      $this->addFile(
        'config/schema/{entity_name}_type.yml',
        'generator/subentity_type.schema.yml'
      );

      $this->addFile(
        '{machine_name}.links.action.yml',
        'generator/links.action.yml.twig'
      );
    }
  }

}
