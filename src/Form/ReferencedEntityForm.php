<?php

namespace Drupal\subentity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for referenced entity edit forms.
 */
class ReferencedEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label %entity_type.', [
          '%label' => $entity->label(),
          '%entity_type' => ucfirst($form['#entity_type']),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label %entity_type.', [
          '%label' => $entity->label(),
          '%entity_type' => ucfirst($form['#entity_type']),
        ]));
    }
  }

}
