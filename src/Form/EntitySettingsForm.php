<?php

namespace Drupal\subentity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for subentity settings form.
 *
 * The FORM_ID must be set in child class.
 */
class EntitySettingsForm extends FormBase {

  /**
   * The unique string identifying the form.
   */
  public const FORM_ID = '';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['entity_settings']['#markup'] = $this->t(
      'Settings form for subentities. Manage field settings here.'
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

}
