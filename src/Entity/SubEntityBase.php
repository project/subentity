<?php

namespace Drupal\subentity\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Class to identify subentities.
 */
abstract class SubEntityBase extends ContentEntityBase {

}
