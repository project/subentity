<?php

namespace Drupal\subentity\Entity\Controller;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a controller to build a listing of Sub entities.
 */
class ReferencedEntityListBuilder extends EntityListBuilder {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ReferencedEntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entity_type_manager = NULL) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $entity_type_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Entity ID');
    $header['uuid'] = $this->t('UUID');
    $header['label'] = $this->t('Label');
    $header['parent'] = $this->t('Parent');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->toLink($entity->id());
    $row['uuid'] = $entity->uuid();
    $row['label'] = $entity->toLink();

    /** @var \Drupal\subentity\Entity\EntityParentHandler $handler */
    $handler = $this->entityTypeManager->getHandler($this->entityTypeId, 'parent');
    $storage_by_entity_type = $handler->getStorageByEntityType();

    foreach ($storage_by_entity_type as $entity_type => $storages) {
      $entity_storage = $this->entityTypeManager->getStorage($entity_type);
      /** @var \Drupal\Core\Entity\Query\QueryInterface $query */
      $query = $entity_storage->getQuery('OR')->accessCheck();

      /** @var \Drupal\field\FieldStorageConfigInterface $storage */
      foreach ($storages as $storage) {
        $query->condition($storage->get('field_name') . '.target_id', $entity->id());
      }

      $result = $query->execute();

      if ($result) {
        $entities = $entity_storage->loadMultiple($result);

        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity_parent */
        foreach ($entities as $entity_parent) {
          $rel = 'canonical';
          $text = $this->getParentText($entity_parent);
          // Some entity types do not have a canonical template link.
          try {
            $row['parent'] = $entity_parent->hasLinkTemplate($rel) ? $entity_parent->toLink($text) : $text;
          }
          catch (UndefinedLinkTemplateException $e) {
            $row['parent'] = $text;
          }
        }
      }
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * Returns the text displayed in the 'Parent' column.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $parent
   *   An entity object.
   *
   * @return string
   *   Text for the 'Parent' column.
   */
  protected function getParentText(ContentEntityInterface $parent): string {
    return '[' . $parent->id() . '-' . $parent->bundle() . '] ' . $parent->label();
  }

}
