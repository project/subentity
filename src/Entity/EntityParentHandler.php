<?php

namespace Drupal\subentity\Entity;

use Drupal\field\FieldStorageConfigStorage;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handler of the parent of the subentity.
 */
class EntityParentHandler implements EntityHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Information about the entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Constructs an entity parent handler instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeInterface $entity_type) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityType = $entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_type.manager'),
      $entity_type
    );
  }

  /**
   * Return config query for entity reference fields.
   *
   * @param \Drupal\field\FieldStorageConfigStorage $field_storage_handler
   *   Storage handler for "field storage" configuration entities.
   *
   * @return \Drupal\Core\Config\Entity\Query\Query
   *   The config entity query.
   */
  protected function getConfigQuery(FieldStorageConfigStorage $field_storage_handler) {
    $query = $field_storage_handler->getQuery();
    return $query
      ->accessCheck()
      ->condition('status', TRUE)
      ->condition('type', ['entity_reference', 'entity_reference_revisions'], 'IN')
      ->condition('settings.target_type', $this->entityType->id());
  }

  /**
   * Finds the storage of reference fields targeting the current entity type.
   *
   * @return array
   *   Associative array:
   *   - Key: entity type id
   *   - Value: list of FieldStorageConfig entities
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStorageByEntityType() {
    /** @var \Drupal\field\FieldStorageConfigStorage $field_storage_handler */
    $field_storage_handler = $this->entityTypeManager->getStorage('field_storage_config');
    $query = $this->getConfigQuery($field_storage_handler);
    $fields = $query->execute();
    if (empty($fields)) {
      return [];
    }

    $storages = $field_storage_handler->loadMultiple($fields);

    // Group fields by entity type so that it will be easier to work with.
    $storage_by_entity_type = [];
    /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
    foreach ($storages as $field_storage) {
      $storage_by_entity_type[$field_storage->getTargetEntityTypeId()][] = $field_storage;
    }

    return $storage_by_entity_type;
  }

}
