CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Usage
  * Technical details
  * Maintainers


INTRODUCTION
------------

  This module supports subentity, a kind of entity which does not exist independently, it always attaches to a parent entity.

  Similar to field_collection in Drupal 7 but for Drupal 8+. Similar to paragraphs but not another paragraph_type.


REQUIREMENTS
------------

  Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


INSTALLATION
------------

  * Install the module using standard module installation.


CONFIGURATION
-------------

  * Configure the user permissions in Administration » People » Permissions:

    - Administer subentities CRUD access for subentities.

      Users with this permission will be able to perform subentity CRUD operations with Admin UI.

  * Perform subentities CRUD operations in Administration » Structure » Subentities.


USAGE
-----

  * Create a new subentity

    ```bash
    drush generate subentity
    ```

    Contrary to the `drush generate content-entity` command, there is no new module
    created with a subentity. A subentity should live in the main entity module.

    It is possible to create a new entity without answering the questions one by
    one. Use the following syntax:

    ```bash
    drush generate subentity --answers='{"machine_name":M,"entity_name":"myentity","entity_class":"MyEntity","label":"My Entity","bundle":"No"}'
    ```

    Then install the database schema in a `hook_update_N`:

    ```php
    function myentity_update_1001() {
      Drupal::entityTypeManager()->clearCachedDefinitions();
      $entity_type = Drupal::entityTypeManager()->getDefinition('myentity');
      Drupal::entityDefinitionUpdateManager()->installEntityType($entity_type);
    }
    ```

TECHNICAL DETAILS
-----------------

  * ReferencedEntityAccessControlHandler

    Access control handler for the referenced entity targeting subentity.
    Subentity does not have any permission on its own, it inherits from the main entity.

  * EntityHtmlRouteProvider

    Provides routes for Sub entities (add-form, settings).

  * ReferencedEntityForm

    Form controller for referenced entity edit forms.

  * EntitySettingsForm

    Common class for entity settings form.

  * ReferencedEntityListBuilder

    Provides a controller to build a listing of Sub entities.


MAINTAINERS
-----------

Current maintainers:
  * Antoine Forgue (anfor) - https://www.drupal.org/user/2577354/
  * Maxime Pahud (MaxPah) - https://www.drupal.org/user/3222928/
  * Hai-Nam Nguyen (jcisio) - https://www.drupal.org/user/210762/
